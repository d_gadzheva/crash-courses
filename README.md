## 1. Ionic with React Crash Course

**Purpose**

Simple BMI Calculator app with Ionic.

**Tools used**

Written on VS Code using React, Ionic and TypeScript.

[Live demo Ionic + React Crash Course](https://1-ionic-react-crash-course.vercel.app) [for better experience => Ctrl + Shift + I => Ctrl + Shift + M (:]

---

## 2. Storyblok Crash Course

**Purpose**

Simple Blog with Storyblok.

**Tools used**

Written on VS Code using Next.js, Storyblok and Vue.js.

[Live demo Storyblok Crash Course](https://story-blog.surge.sh/)

---

## 3. Next.js Crash Course

**Purpose**

Simple Blog with Next.js

**Tools used**

Written on VS Code using Next.js, React and [JSONPlaceholders](https://jsonplaceholder.typicode.com/)

[Live demo Next.js Crash Course](https://3-nextjs-crash-course-dimitriagadzheva-gmailcom.vercel.app/)

---
