import { useRouter } from 'next/router';
import Link from 'next/link';
import React from 'react';
import Meta from '../../../components/Meta';
// import { server } from '../../../config';
import articleStyles from '../../../styles/Articles.module.css';

const article = ({ article }) => {
  // const router = useRouter();
  // const { id } = router.query;

  return (
    <React.Fragment>
      <Meta title={article.title} />
      <div className={articleStyles.article}>
        <h1>{article.title}</h1>
        <p>{article.body}</p>
        <Link href="/">&larr; Go Back</Link>
      </div>
    </React.Fragment>
  );
};

export const getStaticProps = async (context) => {
  const res = await fetch(
    `https://jsonplaceholder.typicode.com/posts/${context.params.id}`
  );

  const article = await res.json();

  return {
    props: {
      article,
    },
  };
};

export const getStaticPaths = async () => {
  const res = await fetch(`https://jsonplaceholder.typicode.com/posts`);

  const articles = await res.json();

  const ids = articles.map((article) => article.id);
  const paths = ids.map((id) => ({ params: { id: id.toString() } }));

  return {
    paths,
    fallback: false,
  };
};

// export const getStaticProps = async (context) => {
//   const res = await fetch(`${server}/api/articles/${context.params.id}`);

//   const article = await res.json();

//   return {
//     props: {
//       article,
//     },
//   };
// };

// export const getStaticPaths = async () => {
//   const res = await fetch(`${server}/api/articles`);

//   const articles = await res.json();

//   const ids = articles.map((article) => article.id);
//   const paths = ids.map((id) => ({ params: { id: id.toString() } }));

//   return {
//     paths,
//     fallback: false,
//   };
// };

export default article;
