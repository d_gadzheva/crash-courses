import ArticleItem from './ArticleItem';
import articlesStyles from '../styles/Articles.module.css';

const ArticleList = ({ articles }) => {
  return (
    <div className={articlesStyles.grid}>
      {articles.map((article) => (
        <ArticleItem key={article.id} article={article} />
      ))}
    </div>
  );
};

export default ArticleList;
